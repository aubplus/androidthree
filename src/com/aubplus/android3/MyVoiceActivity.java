package com.aubplus.android3;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MyVoiceActivity extends Activity {

  private Button startButton;
  private Button stopButton;
  private Button saveVolumeButton;
  private Button sendVolumeButton;

  private TextView volumeTextView;
  private SoundMeter soundMeter;

  private String emailOfMemberString;
  private String nameOfMemberString;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_my_voice);

    startButton = (Button) findViewById(R.id.startButton);
    stopButton = (Button) findViewById(R.id.stopButton);
    saveVolumeButton = (Button) findViewById(R.id.saveVolumeButton);
    sendVolumeButton = (Button) findViewById(R.id.sendVolumeButton);

    volumeTextView = (TextView) findViewById(R.id.volumeTextView);

    soundMeter = new SoundMeter();

    Intent i = getIntent();
    Bundle bundle = i.getExtras();
    emailOfMemberString = bundle.getString("EmAiLLlll");
    nameOfMemberString = bundle.getString("HIS_NAME!!!!");

    sendVolumeButton.setText("Send High Score to " + nameOfMemberString);

    startButton.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        try {
          soundMeter.start();
        } catch (IllegalStateException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    });

    stopButton.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        try {
          soundMeter.stop();
        } catch (IllegalStateException e) {
          e.printStackTrace();
        }
      }
    });

    saveVolumeButton.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        double amplitude = soundMeter.getMaximumAmplitudeSinceLastCall();
        String amplitudeString = "" + amplitude;

        volumeTextView.setText(amplitudeString);
      }
    });

    sendVolumeButton.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        // Implicit Intent! We specify the Action to execute, not a specific Activity. Any
        // app that can execute this Action can be started.
        Intent intent = new Intent(Intent.ACTION_SEND);

        // We put Extras like we did before, with one notable difference: we are using
        // standard keys instead of custom keys (EXTRA_EMAIl, EXTRA_SUBJECT, EXTRA_TEXT).
        // Why?
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {emailOfMemberString});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Look at my Volume High Score");

        String message =
            "Hey " + nameOfMemberString + ",\n"
                + "I wanted to show you my high score! Here it is: "
                + volumeTextView.getText().toString();
        intent.putExtra(Intent.EXTRA_TEXT, message);

        // Try changing the type to "text/html" or "message/rfc822".
        intent.setType("message/rfc822");

        // What if we want to force our users to use the Gmail app and not any other Email app?
        // intent.setClassName("com.google.android.gm",
        // "com.google.android.gm.ComposeActivityGmail");

        startActivity(intent);
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.my_voice, menu);
    return true;
  }

  public class SoundMeter {

    private MediaRecorder mRecorder = null;

    public void start() throws IllegalStateException, IOException {
      if (mRecorder == null) {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mRecorder.setOutputFile("/dev/null");
        mRecorder.prepare();
        mRecorder.start();
      }
    }

    public void stop() {
      if (mRecorder != null) {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
      }
    }

    public double getMaximumAmplitudeSinceLastCall() {
      if (mRecorder != null)
        return mRecorder.getMaxAmplitude();
      else
        return 0;
    }
  }
}

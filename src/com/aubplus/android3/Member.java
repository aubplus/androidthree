package com.aubplus.android3;

public class Member {
  private String firstName;
  private String email;
  private int age;

  public Member(String firstName, String email, int age) {
    this.firstName = firstName;
    this.email = email;
    this.age = age;
  }

  public Member() {
    // Required Empty Constructor.
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }
}

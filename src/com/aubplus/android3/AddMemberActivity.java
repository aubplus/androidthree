package com.aubplus.android3;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddMemberActivity extends Activity {

  // Defining UI Elements:
  private EditText emailEditText;
  private EditText firstNameEditText;
  private EditText ageEditText;
  private Button addMemberButton;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // AddMemberActivity extends the normal Activity, so it should be 'linked' to an XML Layout.
    setContentView(R.layout.activity_add_member);

    // Linking UI Elements:
    emailEditText = (EditText) findViewById(R.id.eET);
    firstNameEditText = (EditText) findViewById(R.id.fnET);
    ageEditText = (EditText) findViewById(R.id.aET);
    addMemberButton = (Button) findViewById(R.id.afButton);

    // Adding an OnClickListener to the addMemberButton.
    addMemberButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        String firstNameString = firstNameEditText.getText().toString();
        String emailString = emailEditText.getText().toString();
        String ageString = ageEditText.getText().toString();

        // If all fields are filled, create an AsyncTask, and execute.
        AddMemberAsyncTask myAsyncTask = new AddMemberAsyncTask();
        myAsyncTask.execute(firstNameString, emailString, ageString);
      }
    });
  }

  @SuppressLint("NewApi")
  private boolean checkThatFieldsAreNotEmpty(String firstNameString, String emailString,
      String ageString) {
    return (firstNameString.isEmpty() || emailString.isEmpty() || ageString.isEmpty());
  }

  // Class with extends AsyncTask class
  private class AddMemberAsyncTask extends AsyncTask<String, Void, String> {

    private ProgressDialog dialog = new ProgressDialog(AddMemberActivity.this);

    protected void onPreExecute() {
      dialog.setMessage("Adding a new member...");
      dialog.show();
    }

    // Call after onPreExecute method
    protected String doInBackground(String... params) {
      // Create a new HttpClient and Post Header
      HttpClient httpclient = new DefaultHttpClient();
      HttpPost httppost = new HttpPost("http://www.aubplus.com/android/add_members.php");

      HttpResponse response = null;
      try {
        // Add your data
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("firstname", params[0]));
        nameValuePairs.add(new BasicNameValuePair("email", params[1]));
        nameValuePairs.add(new BasicNameValuePair("age", params[2]));
        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

        // Execute HTTP Post Request
        response = httpclient.execute(httppost);
        Log.i("Karim", "" + response);
      } catch (Exception e) {
        // TODO Auto-generated catch block
      }

      return response.toString();
    }

    protected void onPostExecute(String response) {
      // Close progress dialog
      dialog.dismiss();
      finish();
    }
  }
}

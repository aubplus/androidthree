package com.aubplus.android3;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MemberArrayAdapter extends ArrayAdapter<Member> {
  private final Context context;
  private final ArrayList<Member> values;

  public MemberArrayAdapter(Context context, ArrayList<Member> values) {
    super(context, R.layout.rowlayout, values);
    this.context = context;
    this.values = values;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
    TextView firstNameTextView = (TextView) rowView.findViewById(R.id.firstNameTextView);
    TextView emailTextView = (TextView) rowView.findViewById(R.id.emailTextView);
    
    firstNameTextView.setText(values.get(position).getFirstName());
    emailTextView.setText(values.get(position).getEmail());

    return rowView;
  }
} 
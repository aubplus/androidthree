package com.aubplus.android3;

import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

public class MembersListActivity extends ListActivity {

  // ArrayList of Friends, which is a List that can be addressed like an Array (using indices).
  private ArrayList<Member> membersArrayList = new ArrayList<Member>();

  // An Adapter object acts as a bridge between an AdapterView (like a ListView) and the underlying
  // data for that view. The Adapter provides access to the data items. The Adapter is also
  // responsible for making a View for each item in the data set.
  private MemberArrayAdapter memberArrayAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Note that there is no 'setContentView' here. This Activity is not tied to an XML layout.

    memberArrayAdapter = new MemberArrayAdapter(this, membersArrayList);

    // Setting the Adapter of the ListView built in the ListActivity.
    setListAdapter(memberArrayAdapter);
  }

  // OnResume is called when the Activity just became visible and can be interacted with.
  @Override
  public void onResume() {
    super.onResume();
    new GetMembersAsyncTask().execute();
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.members_list, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Get ID of MenuItem.
    switch (item.getItemId()) {
      case R.id.action_add:
        // Start Add Member Activity.
        Intent intent = new Intent(MembersListActivity.this, AddMemberActivity.class);
        startActivity(intent);
        return true;
      case R.id.action_refresh:
        new GetMembersAsyncTask().execute();
        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {
    Intent i = new Intent(MembersListActivity.this, MyVoiceActivity.class);
    i.putExtra("HIS_NAME!!!!", membersArrayList.get(position).getFirstName());
    i.putExtra("EmAiLLlll", membersArrayList.get(position).getEmail());
    startActivity(i);
  }
  
  // Class with extends AsyncTask class
  private class GetMembersAsyncTask extends AsyncTask<String, Void, String> {

    private ProgressDialog dialog = new ProgressDialog(MembersListActivity.this);

    protected void onPreExecute() {
      dialog.setMessage("Getting list of members...");
      dialog.show();
    }

    // Call after onPreExecute method
    protected String doInBackground(String... params) {
      // Create a new HttpClient and Post Header
      HttpClient httpclient = new DefaultHttpClient();
      HttpGet httpGet = new HttpGet("http://www.aubplus.com/android/get_members.php");
      httpGet.addHeader("Cache-Control", "no-cache");

      HttpResponse response = null;

      try {
        // Execute HTTP Get Request
        response = httpclient.execute(httpGet);
        String responseBody = EntityUtils.toString(response.getEntity());
        Log.i("Karim", "" + responseBody);
        return responseBody;
      } catch (Exception e) {
        // TODO Auto-generated catch block
      }

      return null;
    }

    protected void onPostExecute(String response) {
      if (response != null) {

        membersArrayList.clear();

        try {
          JSONObject jObject = new JSONObject(response);
          JSONArray jArray = jObject.getJSONArray("Members");
          for (int i = 0; i < jArray.length(); i++) {
            JSONObject oneObject = jArray.getJSONObject(i);
            // Pulling items from the array
            String firstName = oneObject.getString("FirstName");
            String email = oneObject.getString("Email");
            int age = Integer.parseInt(oneObject.getString("Age"));

            Member member = new Member(firstName, email, age);
            membersArrayList.add(member);
          }
        } catch (JSONException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
          Log.e("Karim", e.getMessage().toString());
        }
      }

      memberArrayAdapter.notifyDataSetChanged();

      // Close progress dialog
      dialog.dismiss();
      Log.i("Karim", "dismissed");
    }
  }
}
